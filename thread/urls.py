# core/urls.py

from django.urls import path,include
from core.views import index,thread_detail,create_thread
from django.contrib import admin
urlpatterns = [
    path('', index, name='index'),
    path('admin/', admin.site.urls),
    path('thread/<int:id>/', thread_detail, name='thread_detail'),
    path('thread/new/', create_thread, name='create_thread'),
    path('accounts/', include('accounts.urls')),
]
